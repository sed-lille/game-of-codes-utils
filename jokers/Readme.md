The jokers
==========

## 1) HTTP requests background
For a quickie on HTTP protocol see mozilla's [MDN](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status).


## 2) Recommended workflow 
1. Establish the connection with the bot
2. Implement the API without any behaviour (dummy answers)
3. Design and implement the algorithm for the elevator engine

## 3) HTTP server skeletons in 5 languages

1. [Python - Flask](./jokers/python/flask/). *Credits: Bruno.*
2. [Python - Bottle](./jokers/python/bottle/). *Credits: Julien.*
3. [Java - Spark](./jokers/java/with-sparkjava/). *Credits: Nathalie.*
4. [Pharo](./jokers/pharo/). *Credits: Christophe.*
5. [Matlab](./jokers/matlab/). *Credits: Roudy.*



Matlab Joker(s)
==
Matlab does not provide the HttpServer functionality required for the contest. However, Matlab interfaces natively with Java, which make it possible to reuse any HTTP server java code. This joker provides you with all the inputs to achieve such simple, but not straightforward, goal.  
**Warning:**
All code (Java & Matlab) is homemade, and not verified intensively. Also, the java interface of Matlab is not (fully) documented, especially regarding the event callback interface.  
**Danger:**
Crossing fingers for this to work on other Matlab releases than the one we used (i.e R2015b).


# Joker1: Basic java-based Http Server
This joker provides you with Java library (.jar file) and some Matlab code that allows you to handle incoming HTTP requests on a given port.

1. Open your Matlab :)
2. Download the required code (matlab and java lib) as below:

    ```matlab
    >> websave('J1.zip','https://gitlab.inria.fr/sed-lille/httpd-matlab/uploads/34cd0acd7c60f7c70db8d54db0f8e578/J1.zip')
    >> unzip('J1.zip')
    ```

3. Setup Matlab static classpath to include `./lib/httpd-matlab-0.0.1.jar`

    ```matlab
    >> setup('httpd-matlab-0.0.1.jar')
    ```

3. Reboot matlab
4. Check your installation by calling `which httpd.HttpEngine`

    ```matlab
    >> which httpd.HttpEngine
    httpd.HttpEngine is a Java method  % httpd.HttpEngine constructor
    ```
5. Run Simple tests: this will launch a server on port 8080 with the required HTTP contexts for the elevator engine.

    ```matlab
    >> run test/test.m
    ```
    Then you can interact with the server via your browser.
  
The Matlab HTTP wrapper is available in the `httpServer.m` Matlab class. A complete example is also provided in the `./test/` folder. Have a look !


# Joker2: Ready to use skeleton class
This Joker extends Joker 1 with some string manipulation tricks for parsing the HTTP request from the bot.   
A Matlab class (httpElevatorEngine.m) is provided as a skeleton for the elevator engine. It wraps a httpServer.m object and provide handlers for the following incoming HTTP requests (or contexts):

- `/userHasExited`
- `/userHasEntered`
- `/go?floorToGo=[0-5]`
- `/call?atFloor=[0-5]&to=[UP|DOWN]` 
- `/nextCommand`
- `/reset?cause=information+message`

All what you have to do is implement the callbacks for the requests. 

1. Setup and tests: follow the operations in Joker 1
2. In the same working directory download the `httpElevatorEngine.m` Matlab class:

    ```matlab
    >> websave('httpElevatorEngine.m','https://gitlab.inria.fr/sed-lille/httpd-matlab/uploads/fd6979ba8d3a6d136607e95d30b88700/httpElevatorEngine.m')
    ```
3. Test the API compliancy

    ```matlab
    >> engine =  httpElevatorEngine(8080)
        engine = 
    
      httpElevatorEngine with properties:
    
        mHttpServer: [1x1 httpServer]
               port: 8080
           contexts: {'/userHasExited'  '/userHasEntered'  '/go'  '/call'  '/nextCommand'  '/reset'}
    >> engine.status()
    Server is up
     
    srv_contexts =
     
    [/userHasExited, /userHasEntered, /go, /call, /nextCommand, /reset]
    
    >> engine.start()
    Server is running on http:// /0:0:0:0:0:0:0:0:8080
    
    % Interact with the server according to the API
    %  using your browser or any command-line tool
    % Stop the server (always do this before clearing the variable 'engine'
    engine.stop()
    ```
4. Update the code of the callbacks to implement your strategy (follow the #TODO#GoC). Any state variables can be added in the `properties` section of the class.

# Important note
If you want to remove the installed java library from your path, you would have to remove the corresponding line from the Java static path

```matlab
open([prefdir '/javaclasspath.txt'])
```
and restart Matlab.


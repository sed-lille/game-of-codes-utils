The easiest way to set up a ReST server with Pharo is to use [Teapot](http://smalltalkhub.com/#!/~zeroflag/Teapot).

You can install it with the following expression:
```
Metacello new
  smalltalkhubUser: 'zeroflag' project: 'Teapot';
  configuration: 'Teapot';
  load.
```

To set up the code-elevator participant (a ReST server), use the following code:
```
server := (Teapot 
		configure: {
		   #defaultOutput -> #text .
			#port -> 2000 .
			#bindingAddress -> 'O.O.O.O'.
			#debugMode -> true })
    GET: '/call' -> [:req | TeaResponse ok];
    GET: '/go' -> [:req | TeaResponse ok];
    GET: '/userHasEntered' -> [:req | TeaResponse ok];
    GET: '/userHasExited' -> [:req | TeaResponse ok];
	GET: '/reset' -> [:req | TeaResponse ok];
	GET: '/nextCommand' -> [:req | TeaResponse ok body: #NOTHING];
	yourself.
server start
```
